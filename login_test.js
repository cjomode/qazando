Feature('login');

Scenario('Login com sucesso',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/')
    I.click('Login')
    // I.click('#user') //
    I.waitForText('Login', 10)
    I.fillField('#user','testeqazando@mailinator.com')
    I.fillField('#password','123456')
    I.click('#btnLogin')
    I.waitForText('Login realizado', 3)

}).tag('@sucesso')

Scenario('Tentando Logar digitando apenas o e-mail',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login')
    I.fillField('#user', 'testeqazando@mailinator.com')
    I.click('#btnLogin') 
    I.waitForText('Senha inválida.', 3)

}).tag('@fail_password_empty')

Scenario('Tentando logar sem digitar e-mail e senha',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.', 3)

}).tag('@fail_both_empty')

Scenario('Tentando Logar digitando apenas a senha',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login')
    I.fillField('#password','123456')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.', 3)

}).tag('@fail_email_empty')

